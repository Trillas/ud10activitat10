package dto;

import exceptions.excepciones;

public class Metodos {
	//Un metodo para generar un n�mero aleatorio
	public static int numAleatorio(int min, int max) {
		int num;
		num = (int)(Math.random() * ((max - min) + 1) + min);
		return num;
	}
	
	//Un m�todo para comprarar el n�mero con el buscado
	public static boolean mayorIgualMenor(int num, int numBuscado) {
		 	if (num > numBuscado) {
				System.out.println("El n�mero es mas peque�o");
				return false;
			}else if(num < numBuscado) {
				System.out.println("El n�mero es mas grande");
				return false;
			}else {
				System.out.println("Has acertado el n�mero");
				return true;
			}
	}

	//Ejecuta una partida
	//Aqui voy llamando a los otros m�todos para crear la partida, tambien llamo al trycatch que es donde genero el numero
	public static void ejecutarPartida() {
		int intentos = 0;
		int numAleatorio = numAleatorio(1, 500);
		//Ense�o el n�mero para poder hacer pruebas
		System.out.println(numAleatorio);
		int num;
		boolean salir = false;
		do {
			num = excepciones.tryCatch();
			if (mayorIgualMenor(num, numAleatorio)) {
				System.out.println("Has hecho " + intentos + " intentos");
				salir = true;
			}
			intentos ++;
		} while (!salir);
	}
}
